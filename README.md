# Infrastructure Module - Amazon SES

This reusable infrastructure module supports [Amazon SES](https://aws.amazon.com/ses/) domain verification and DKIM.
