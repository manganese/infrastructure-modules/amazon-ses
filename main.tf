locals {
  has_subdomain = var.subdomain != ""
  subdomain_prefix = local.has_subdomain ? "${var.subdomain}." : ""
  subdomain_suffix = local.has_subdomain != "" ? ".${var.subdomain}" : ""
}

resource "aws_ses_domain_identity" "domain_main" {
  domain = "${local.subdomain_prefix}${var.domain_main}"
}

resource "aws_route53_record" "aws_ses_domain_main_verification_record" {
  zone_id = var.aws_zone_id
  name = "_amazonses${local.subdomain_suffix}"
  type = "TXT"
  ttl = "3600"
  records = [
    aws_ses_domain_identity.domain_main.verification_token
  ]
}

resource "aws_ses_domain_dkim" "domain_main_dkim" {
  domain = aws_ses_domain_identity.domain_main.domain
}

resource "aws_route53_record" "aws_ses_domain_main_dkim_record" {
  count = 3
  zone_id = var.aws_zone_id
  name = "${element(aws_ses_domain_dkim.domain_main_dkim.dkim_tokens, count.index)}._domainkey${local.subdomain_suffix}"
  type = "CNAME"
  ttl = "3600"
  records = [
    "${element(aws_ses_domain_dkim.domain_main_dkim.dkim_tokens, count.index)}.dkim.amazonses.com"
  ]
}
