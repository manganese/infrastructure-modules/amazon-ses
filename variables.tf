# AWS
variable "aws_zone_id" {
  type = string
  description = "The AWS Route53 zone ID"
}

# DNS
variable "domain_main" {
  type = string
  description = "The main domain name"
}

variable "subdomain" {
  type = string
  description = "The subdomain under the main domain, used to identify the identity in the DNS zone"
  default = ""
}